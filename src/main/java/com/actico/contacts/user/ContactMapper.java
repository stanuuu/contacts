package com.actico.contacts.user;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.actico.contacts.contact.Contact;
import com.actico.contacts.contact.ContactView;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ContactMapper {

  Contact toContact(ContactView contact);
  ContactView toContactView(Contact contact);
  List<ContactView> toContactViewsList(List<Contact> contacts);

  default Page<ContactView> toUserViewPage(Page<Contact> entityPage){
    return new PageImpl<>(
        this.toContactViewsList(entityPage.getContent()),
        entityPage.getPageable(),
        entityPage.getTotalElements());
  }
}

