package com.actico.contacts.user;

import com.actico.contacts.contact.ContactView;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserView {
  Long id;
  Long version;
  String email;
  String firstName;
  String lastName;
  String displayName;
  Set<ContactView> contacts;
}
