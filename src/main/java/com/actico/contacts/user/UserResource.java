package com.actico.contacts.user;

import com.actico.contacts.contact.ContactView;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UserResource {

  ContactService service;

  @GetMapping("/{userId}/contacts")
  public Page<ContactView> getAll(@PathVariable Long userId,
      final @PageableDefault(size = 10, sort = "id") Pageable pageable) {
    return service.getAll(userId, pageable);
  }

  @PostMapping("/{userId}/contacts")
  @ResponseStatus(HttpStatus.CREATED)
  public ContactView create(@PathVariable Long userId, @RequestBody @Validated ContactView contact) {
    return service.create(userId, contact);
  }

  @DeleteMapping("/{userId}/contacts/{contactId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Long userId, @PathVariable Long contactId) {
    service.delete(userId, contactId);
  }

}
