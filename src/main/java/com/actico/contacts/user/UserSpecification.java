package com.actico.contacts.user;

import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Conjunction;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;

@Join(path = "contacts", alias = "contact")
@Conjunction({
    @Or({
        @Spec(path = "contact.id", params = "retailer", spec = Equal.class),
        @Spec(path = "contact.name", params = "retailer", spec = LikeIgnoreCase.class),
        @Spec(path = "contact.firstName", params = "retailer", spec = LikeIgnoreCase.class),
        @Spec(path = "contact.lastName", params = "retailer", spec = LikeIgnoreCase.class)
    })
})
public interface UserSpecification extends Specification<User> {

}
