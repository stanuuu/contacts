package com.actico.contacts.user;

import static lombok.AccessLevel.PRIVATE;

import com.actico.contacts.contact.ContactRepository;
import com.actico.contacts.contact.ContactView;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ContactService {

  UserRepository userRepository;
  ContactRepository contactRepository;
  ContactMapper mapper;

  public Page<ContactView> getAll(Long userId, Pageable pageable) {
    return mapper.toUserViewPage(contactRepository.findContactByUserId(userId, pageable));
  }

  public ContactView create(Long userId, ContactView contact) {
    return null;
//    userRepository.findById(userId)
//        .map(user -> user.getContacts().add(mapper.toContact(contact)));
//        .orElseThrow(); throw custom exception
  }

  public void delete(Long userId, Long contactId) {
//    contactRepository.findById(contactId)
//        .map(contact -> {
//          // check if contact owned by user
//          //
//        })
//        .orElseThrow(); // throw custom exception
  }

}
