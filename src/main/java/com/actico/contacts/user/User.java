package com.actico.contacts.user;

import static lombok.AccessLevel.PRIVATE;

import com.actico.contacts.contact.Contact;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = PRIVATE)
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "user")
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Version
    Long version;

    @NotNull
    String email;

    String firstName;
    String lastName;
    String displayName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", orphanRemoval = true)
    public List<Contact> contacts;

    @PrePersist
    public void prePersistContacts() {
        contacts.forEach(contact -> contact.setUser(this));
    }

    @PreRemove
    public void preRemoveContacts() {
        contacts.forEach(contact -> contact.setUser(null));
    }

}
