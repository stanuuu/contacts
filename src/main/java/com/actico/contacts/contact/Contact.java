package com.actico.contacts.contact;

import static lombok.AccessLevel.PRIVATE;

import com.actico.contacts.user.User;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = PRIVATE)
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "contact")
@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Version
    Long version;

    String email;

    @NotNull
    String firstName;

    String lastName;
    String displayName;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    public User user;
}
