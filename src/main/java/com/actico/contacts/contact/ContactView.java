package com.actico.contacts.contact;

import com.actico.contacts.user.UserView;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class ContactView {
  Long id;
  Long version;
  String email;
  String firstName;
  String lastName;
  String displayName;
  UserView user;
}
